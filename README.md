<div align="center">
  <img src="images/icon.png" width="200" height="200">
 <h1>Electro Parrot</h1>
 <p>
  <b>A dark theme for VS Code</b>
 </p>
 <br>
 <br>
 <br>
</div>

##  About

Electro Parrot is a dark theme for VS Code.

##  Examples

##  Golang

![Golang](images/example_go.png)

## JSX/TSX

![JSX/TSX](images/example_tsx.png)

## Vue

![Vue](images/example_vue.png)
